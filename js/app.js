// alapszamok generalasa
function alapszamok_feltolt() {
  const alapszam_tarolt = JSON.parse(localStorage.getItem('settings-alapszam'));
  var settings = document.getElementById('alapszamok');
  for (i = 1; i <= 10; i++) {
    var checkbox_wrapper = document.querySelector('#templates .form-check').cloneNode(true);
    checkbox_wrapper.querySelector('input').setAttribute('name', 'settings-alapszam');
    checkbox_wrapper.querySelector('input').setAttribute('value', i);
    checkbox_wrapper.querySelector('input').setAttribute('id', 'settings-alapszam-' + i);
    if (alapszam_tarolt != null && alapszam_tarolt.includes(i.toString())) {
      checkbox_wrapper.querySelector('input').setAttribute('checked', 'checked');
    }
    checkbox_wrapper.querySelector('label').htmlFor = 'settings-alapszam-' + i;
    checkbox_wrapper.querySelector('label').textContent = i;
    settings.appendChild(checkbox_wrapper);
  }
}

// alapszamok generalasa
function muveletek_feltolt() {
  const muvelet_tarolt = JSON.parse(localStorage.getItem('settings-muvelet'));
  var settings = document.getElementById('muveletek');
  ['*', '/'].forEach(function (value) {
    var checkbox_wrapper = document.querySelector('#templates .form-check').cloneNode(true);
    checkbox_wrapper.querySelector('input').setAttribute('name', 'settings-muvelet');
    checkbox_wrapper.querySelector('input').setAttribute('value', value);
    checkbox_wrapper.querySelector('input').setAttribute('id', 'settings-muvelet-' + value);
    if (muvelet_tarolt != null && muvelet_tarolt.includes(value)) {
      checkbox_wrapper.querySelector('input').setAttribute('checked', 'checked');
    }
    checkbox_wrapper.querySelector('label').htmlFor = 'settings-alapszam-' + value;
    checkbox_wrapper.querySelector('label').textContent = value;
    settings.appendChild(checkbox_wrapper);
  })
}

// Üzenetek törlése timeout után
function delete_messages() {
  var messages = document.getElementById('messages');
  setTimeout(() => {
    messages.className = 'alert';
    messages.textContent = '';
  }, 3000)
}

// üzenet beállítása
function set_message(message, type) {
  messages.classList.add('alert-' + type);
  messages.textContent = message;
  delete_messages();
}

// beállítások form beküldése
function settings_form_submit() {
  const event = this.event;
  event.preventDefault();
  switch (event.submitter.name) {
    case 'settings-save':
      beallitasok_mentese();
      break;
    case 'settings-reset':
      beallitasok_torlese();
      break;
  }
}

// beállítások ellenőrzése
function beallitasok_ellenorzese(formData) {
  const alapszam = formData.getAll('settings-alapszam');
  const muvelet = formData.getAll('settings-muvelet');
  return (alapszam.length !== 0 && muvelet.length !== 0)
}

// beállítások mentése
function beallitasok_mentese() {
  const formData = new FormData(document.getElementById('settings'));
  if (beallitasok_ellenorzese(formData)) {
    localStorage.setItem('settings-alapszam', JSON.stringify(formData.getAll('settings-alapszam')));
    localStorage.setItem('settings-muvelet', JSON.stringify(formData.getAll('settings-muvelet')));
    set_message('Beállítások mentése megtörtént.', 'success');
  }
  else {
    set_message('Legalább egy alapszámot és egy műveletet ki kell választani!', 'danger');
  }
}

// beállítások törlése
function beallitasok_torlese() {
  localStorage.removeItem('settings-alapszam');
  localStorage.removeItem('settings-muvelet');
  var checkboxes = document.getElementById('settings').querySelectorAll('input[type="checkbox"][checked="checked"]');
  checkboxes.forEach(function (checkbox) {
    checkbox.removeAttribute('checked');
  });
  var messages = document.getElementById('messages');
  messages.classList.add('alert-success');
  messages.textContent = 'Beállítások törölve.';
  delete_messages();
}

//
// MAIN
//
alapszamok_feltolt();
muveletek_feltolt();
